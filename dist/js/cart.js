$(function() {
    var cartArray = JSON.parse(localStorage.getItem('cart')) || [];

    if (cartArray.length === 0) {
        $('#empty-cart').fadeIn();
    } else {
        var $cartTable = $('#cart-table-body');
    
        $(cartArray).each(function(ind, el) {
            var image = '<td class="cart__item-img"><img src="'+ el.image +'" alt="'+ el.name +'"></td>';
            var name = '<td class="cart__item-name">'+ el.name +'</td>';
            var price = '<td class="cart__item-price">'+ el.price +'</td>';
            var amount = [
                '<td class="cart__item-amount">',
                    '<div class="js-amount cart__item-amount-control">',
                        '<span class="js-amount-minus cart__item-amount-control-minus">',
                            '<span class="icon icon-back"></span>',
                        '</span>',
                        '<div class="js-amount-number cart__item-amount-control-number" data-amount="'+ el.amount +'">'+ el.amount +'</div>',
                        '<span class="js-amount-plus cart__item-amount-control-plus">',
                            '<span class="icon icon-next"></span>',
                        '</span>',
                    '</div>',
                '</td>',
            ].join("\n");
            var removeItem = '<td class="cart__item-remove js-remove-cart-item"><button class="close-btn">✕</button></td>';
    
            var $itemContainer = $('<tr class="cart__item">');
            $itemContainer.append(image);
            $itemContainer.append(name);
            $itemContainer.append(price);
            $itemContainer.append(amount);
            $itemContainer.append(removeItem);
    
            $cartTable.append($itemContainer);
        });

        $('#cart').fadeIn();
        $('#cart-form').fadeIn();
    }

})