function isMobile() { 
    if (
        navigator.userAgent.match(/Android/i) || 
        navigator.userAgent.match(/webOS/i) ||
        navigator.userAgent.match(/iPhone/i) ||
        navigator.userAgent.match(/iPad/i) ||
        navigator.userAgent.match(/iPod/i) ||
        navigator.userAgent.match(/BlackBerry/i) ||
        navigator.userAgent.match(/Windows Phone/i)
    ) {
        return true;
    };
}

if (isMobile()) {
    document.body.classList.add('ua-mobile');
}

//fake AJAX
(function($){
    $.ajax = function(params){
        if(params.url === '/fake_cart') {
            params.success({
                "total_items": "1"
            });
        }
    };
})(jQuery);
//fake AJAX />

$(function() {
    window.Armata = {
        notification: function (message, duration) {
            var $new_message = $('<div class="notification"></div>'),
                $opened_messages = $('.notification');

            $new_message.html(message);

            if ($opened_messages.length) {
                var $last_message = $opened_messages.last(),
                    top_offset = $last_message[0].offsetTop + $last_message.outerHeight() + 15;

                $new_message.css('top', top_offset + 'px')
            }

            $new_message.appendTo('body');
            $new_message.css('opacity'); //reflow hack for transition
            $new_message.addClass('notification--show');

            if (duration) {
                setTimeout(function () {
                    $new_message.fadeOut(function () {
                        $new_message.remove();
                    })
                }, duration);
            }

            $('.notification').click(function () {
                $(this).fadeOut(function () {
                    $new_message.remove();
                })
            });
        }
    };



    //Фиксированное меню на телефонах
    if (isMobile()) {
        var $menu = $('.menu'),
            menuHeight = $menu.outerHeight(),
            headerHeight = $('.page-header').outerHeight(),
            $pageWrap = $('.page-wrapper');

        $(window).scroll(function () {
            if ($(this).scrollTop() > headerHeight) {
                $menu.addClass('menu--fixed');
                $pageWrap.css('padding-top', menuHeight);
            }
            else {
                $menu.removeClass('menu--fixed');
                $pageWrap.css('padding-top', '0');
            }
        })
    }




    //button preloader УДАЛИТЬ
    $('form .btn').click(function (e) {
        e.preventDefault();
        var $this = $(this);

        $this.addClass('loading-btn');
        setTimeout(function () {
            $this.removeClass('loading-btn');
            window.Armata.notification('Сообщение отправлено!');
        }, 3000);
    });
    //button preloader demo />



    //menu toggle
    $('#menu-toggle').click(function() {
        $('#menu-list').addClass('menu__list--open');
        $('body').css('overflow', 'hidden');
    });

    $('#close-menu').click(function() {
        $('#menu-list').removeClass('menu__list--open');
        $('body').css('overflow', 'auto');
    });



    //sidebar toggle />
    $('#open-sidebar').click(function() {
        $('#sidebar').addClass('sidebar--open');
        $('body').css('overflow', 'hidden');
    });

    $('#close-sidebar').click(function() {
        $('#sidebar').removeClass('sidebar--open');
        $('body').css('overflow', 'auto');
    });



    //filters toggle
    $('#js-filters-toggle').click(function () {
        var $this = $(this);

        $('.filters-form').slideToggle(function() {
            $this.toggleClass('filters-toggle--active');
        });
    });



    //reset filter
    $('.js-reset-filter').click(function(e) {
        e.preventDefault();

        if (window.location.search) {
            window.location.href = window.location.href.split('?')[0]
        }
        else {
            $(this).closest('form')[0].reset()
        }
    })



    //form submit agreement
    var $checkbox = $('.js-agreement-check'),
        $submitBtn = $checkbox.closest('.form__submit').find('button');

    $checkbox.prop('checked', true);

    $checkbox.click(function() {
        var $this = $(this),
            $btn = $checkbox.closest('.form__submit').find('button');

        $this.prop('checked') ?
            $btn.prop('disabled', false) :
            $btn.prop('disabled', true);
    });



    //показать пользовательское соглашение
    $('#js-agreement-open').click(function () {
        $('#js-agreement-popup').addClass('page-overlay--show')
    });



    //amount control span
    $('.js-amount-minus').click(function() {
        var $total = $(this).siblings('.js-amount-number'),
            newValue = +$total.data('amount') - 1;

        newValue = (newValue < 1) ? 1 : newValue;

        $total
            .data('amount', newValue)
            .attr('data-amount', newValue)
            .html(newValue);
    });

    $('.js-amount-plus').click(function() {
        var $total = $(this).siblings('.js-amount-number'),
            newValue = +$total.data('amount') + 1;

        $total
            .data('amount', newValue)
            .attr('data-amount', newValue)
            .html(newValue);
    });



    //product card popup
    $('.js-product-card-popup').click(function() {
        $('#js-product-popup').addClass('page-overlay--show');
        $('body').css('overflow', 'hidden');

        var product_data = $(this).data('product-info');

        $('#js-product-popup__title').html(product_data.name + ' ' + product_data.specs);
        $('#js-product-popup__img').attr('src', product_data.img);
        if (product_data.price) {
            $('#js-product-popup__price').html(product_data.price);
        } 
        else {
            var noPrice = $('<div class="product-popup__no-price">Цена по запросу</div>');
            $('#js-product-popup__price').html(noPrice);
        }

        $('#js-product-popup__info').empty();

        $.each(product_data.info, function(ind, item) {
            var row = $('<tr></tr>'),
                col = $('<td></td>')
                value = $('<b></b>').text(item.value);

            col.text(item.name + ' ').append(value);
            row.append(col);

            $('#js-product-popup__info').append(row);
        });

        var in_stock_row = '';
        if (product_data.in_stock) {
            in_stock_row = $('<tr class="product-info__in-stock"><td>В наличии на складе</td></tr>');
        }

        $('#js-product-popup__info').append(in_stock_row);
        $('.product-popup__amount-total')
            .text(1)
            .data('amount', 1)
            .attr('data-amount', 1);

        $('#js-product-popup__description').empty();
        $.each(product_data.description, function(ind, item) {
            var container = $('<div class="product-description__item"></div>'),
                container_title = $('<div class="product-description__title"></div>').html(item.title),
                list = $('<ul class="product-description__list"></ul>');

            $.each(item.list, function(ind, el) {
                var new_item = $('<li></li>').text(el.item);
                list.append(new_item);
            })

            container
                .append(container_title)
                .append(list);

            $('#js-product-popup__description').append(container);
        });

        $('#js-product-popup__submit')
            .data('item-id', product_data.item_id)
            .attr('data-item-id', product_data.item_id);
    });



    //hide-popup
    $(document).mouseup(function(e) {
        if (
            !$(".popup").is(e.target) && 
            $(".popup").has(e.target).length === 0 ||
            $(".popup__close").is(e.target)
        ) {
            $('.page-overlay').removeClass('page-overlay--show');
            $('body').css('overflow', 'auto');
        }
    });



    //remove cart item
    $('.js-remove-cart-item').click(function() {
        var item_amount = +$(this).closest('tr').data('init-amount');

        if ($('.cart__item').length === 1) {
            $('#cart').remove();
            $('#empty-cart').show();
        } else {
            $(this).closest('.cart__item').remove();
        };
    });



    //input type file
    $('#js-file-field input[type="file"]').on('change', function() {
        var filename = $(this).val();
        filename = filename.replace(/.*[\\\/]/, '../');

        (filename) ?
            $('#js-file-field-caption').html(filename) :
            $('#js-file-field-caption').html('Прикрепите резюме');
    });



    //toggle testimonials
    $('#toggle-testimonials').click(function() {
        $(this).toggleClass('testimonials-control__more--active');

        var $caption = $('#testimonials-toggle-text');
            new_text = $caption.data('swap-text'),
            old_text = $caption.text();
            
            $caption.text(new_text);
            $caption.data('swap-text', old_text);

        $('#hidden-testimonials').slideToggle();
    });



    //open/close testimonial popup
    $('#open-testimonial-popup').click(function() {
        $('#testimonial-popup').addClass('testimonial-popup--open');
        $('body').css('overflow-y', 'hidden');
    });
    $('#close-testimonial-popup').click(function() {
        $('#testimonial-popup').removeClass('testimonial-popup--open');
        $('body').css('overflow-y', 'auto');
    });
});

$(window).load(function() {
    //equal image-caption size
    var $categoryCaption = $('.img-figures__item-caption'),
    captionMaxLength = 0;

    $.each($categoryCaption, function(ind, elem) {
        if ($(elem).outerWidth() > captionMaxLength) captionMaxLength = $(elem).outerWidth();
    })
    $.each($categoryCaption, function(ind, elem) {
        $(elem).css({
            "width": "100%",
            "max-width": captionMaxLength + "px"
        });
    });
});