$(function() {
    var cartArray = JSON.parse(localStorage.getItem('cart')) || [];

    var itemData = {
        item_id: null,
        image: null,
        name: null,
        price: null,
        amount: null
    };

    $('.js-add-to-cart').click(function() {
        var $this = $(this);
        itemData.item_id = $this.data('item-id');
        itemData.image = $('#js-product-popup__img').attr('src');
        itemData.name = $('#product-page-title').text() || $('#js-product-popup__title').text();
        itemData.price = $('#js-product-popup__price').text();
        itemData.amount = $this.closest('.js-submit-block').find('.js-amount-number').data('amount');
        
        addToCart(itemData);
    });

    function addToCart(newItem) {
        cartArray = JSON.parse(localStorage.getItem('cart')) || [];

        if (cartArray.length === 0) {
            console.log('LENGTH0');
            cartArray.push(newItem);
            localStorage.setItem('cart', JSON.stringify(cartArray));
        }
        else {
            var alreadyAdded = false;

            $(cartArray).each(function (ind, itemInCart) {
                if (newItem.item_id == itemInCart.item_id) {
                    console.log('EQUALS - ID_OLD:' + itemInCart.item_id + ' ID_NEW:' + newItem.item_id);
                    itemInCart.amount = itemInCart.amount + newItem.amount;
                    localStorage.setItem('cart', JSON.stringify(cartArray));
                    alreadyAdded = true;
                    return;
                }
            })

            if (!alreadyAdded) {
                cartArray.push(newItem);
                localStorage.setItem('cart', JSON.stringify(cartArray));
            }
        }
    };
})