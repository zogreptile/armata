var gulp 			= require('gulp'),
	scss 			= require('gulp-sass'),
	bulkSass 		= require('gulp-sass-glob-import'),
	rename 			= require('gulp-rename'),
	bs 				= require('browser-sync').create(),
	autoprefixer 	= require('gulp-autoprefixer'),
	cssmin 			= require('gulp-cssmin'),
	combineMq 		= require('gulp-combine-mq'),
	sourcemaps 		= require('gulp-sourcemaps'),
	nunjucks 		= require('gulp-nunjucks'),
	data 			= require('gulp-data'),
	paths 			= require('./package.json').paths,
	browserslist 	= require('./package.json').browserslist;

gulp.task('nunjucks', function() {
	gulp.src(paths.src.templates + '*.html')
		.pipe(data(function() {
			return require('./src/_data/_global.json');
		}))
		.pipe(data(function() {
			return require('./src/_data/catalog-tires.json');
		}))
		.pipe(data(function() {
			return require('./src/_data/catalog-disc.json');
		}))
		.pipe(data(function() {
			return require('./src/_data/catalog-chains.json');
		}))
		.pipe(nunjucks.compile())
		.pipe(gulp.dest(paths.dist.html))
		.pipe(bs.reload({ stream: true }));
	}
);

gulp.task('scss', function () {
	return gulp.src(paths.src.styles + 'main.scss')
		.pipe(sourcemaps.init())
		.pipe(bulkSass())
		.pipe(scss().on('error', scss.logError))
		.pipe(scss({
			includePaths: [paths.src.styles]
		}))
		.pipe(sourcemaps.write())
		.pipe(rename('style.css'))
		.pipe(gulp.dest(paths.dist.css))
		.pipe(bs.reload({stream: true}));
});

gulp.task('demo-css', function() {
	return gulp.src(paths.src.styles + 'demo.scss')
		.pipe(bulkSass())
		.pipe(scss().on('error', scss.logError))
		.pipe(scss({
			includePaths: [paths.src.styles]
		}))
		.pipe(autoprefixer({
			browsers: ['last 8 versions'],
			cascade: false
		}))
		.pipe(rename('demo-style.css'))
		.pipe(gulp.dest(paths.dist.css))
		.pipe(bs.reload({stream: true}));
});

gulp.task('browser-sync', function() {
	bs.init({
		server: {
			baseDir: './dist'
		}
	});
});

gulp.task('autoprefixer', function() {
	gulp.src(paths.dist.css + 'style.css')
		.pipe(autoprefixer({
			browsers: browserslist,
			cascade: false
		}))
		.pipe(gulp.dest(paths.dist.css));
});

gulp.task('cssmin', function () {
	gulp.src(paths.dist.css + 'style.css')
		.pipe(bulkSass())
		.pipe(scss({
			includePaths: [paths.src.styles]
		}))
		.pipe(cssmin())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest(paths.dist.css));
});

gulp.task('css', function() {
	gulp.src(paths.dist.css + 'style.css')
		.pipe(bulkSass())
		.pipe(scss({
			includePaths: [paths.src.styles]
		}))
		.pipe(autoprefixer({
			browsers: ['last 8 versions'],
			cascade: false
		}))
		.pipe(combineMq({
			beautify: true
		}))
		.pipe(cssmin())
		.pipe(rename('style.min.css'))
		.pipe(gulp.dest(paths.dist.css));
});

gulp.task('watcher', function() {
	gulp.watch(paths.src.templates + '**/*.html', ['nunjucks']);
	gulp.watch(paths.src.styles + '**/*.scss', ['scss']);
});

gulp.task('default', ['watcher', 'browser-sync']);